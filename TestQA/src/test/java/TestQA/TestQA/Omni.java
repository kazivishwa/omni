package TestQA.TestQA;

import org.testng.annotations.Test;

import CommonFuntions.LoginDetails;
import CommonFuntions.Utility;
import pageObjectRepositiory.DocumentExplorer;

import org.testng.annotations.BeforeTest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.DependencyMap;
import org.testng.annotations.AfterTest;

public class Omni {
	WebDriver driver = null;
	String browser;
	String userName;
	String password;
	String docName;
	
@BeforeTest
	  public void beforeTest() throws IOException, InterruptedException {
	Properties prop = new Properties();
	FileInputStream ip = new FileInputStream("src\\test\\java\\TestQA\\TestQA\\config.properties");
	prop.load(ip);	
	browser=prop.getProperty("browser");
	userName=prop.getProperty("username");
	password=prop.getProperty("password");
	docName=prop.getProperty("documentName");
	
	
	
	if(browser=="chrome"){
		System.setProperty("webdriver.chrome.driver","C:\\Users\\Arun XPS\\Downloads\\chromedriver.exe");
		driver = new ChromeDriver();
	}
	else if(browser.equals("FF")){
		System.setProperty("webdriver.gecko.driver", "C:\\Users\\Arun XPS\\Downloads\\geckodriver.exe");
		driver = new FirefoxDriver();
		
	}
	else{
		System.out.println("no browser value is given");
	}
	
	driver.manage().deleteAllCookies();
	driver.manage().window().maximize();
	driver.get(prop.getProperty("url"));
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	 
	  }

  @Test()
  public void CreateDocument() throws InterruptedException {
	  LoginDetails.login(driver, userName, password);	 
	  //DocumentExplorer.addNewCollection(driver);
	  //DocumentExplorer.createName(driver,docName);
	  //DocumentExplorer.clickToCreatCollection(driver);	   
	  DocumentExplorer.searchCreatedCollection(driver,docName);	  
	  DocumentExplorer.selectAListFromSearchResult(driver,docName);
	  DocumentExplorer.uploadFile(driver);
	  DocumentExplorer.searchDocument(driver);
  }
 
  @AfterTest
  public void afterTest() {
	//  driver.quit();
	  
  }

}

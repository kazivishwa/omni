package TestQA.TestQA;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;



public class ReadProp {

	public static void main(String[] args) throws IOException, InterruptedException{
		
		WebDriver driver = null;
		
		Properties prop = new Properties();
		FileInputStream ip = new FileInputStream("C:\\Users\\Arun XPS\\eclipse-workspace\\Omni\\TestQA\\"
				+ "src\\test\\java\\TestQA\\TestQA\\config.properties");
		
		prop.load(ip);
		
		System.out.println(prop.getProperty("browser"));
		String browserName = prop.getProperty("browser");
				
				if(browserName=="chrome"){
					System.setProperty("webdriver.chrome.driver","C:\\Users\\Arun XPS\\Downloads\\chromedriver.exe");
					driver = new ChromeDriver();
				}
				else if(browserName.equals("FF")){
					System.setProperty("webdriver.gecko.driver", "C:\\Users\\Arun XPS\\Downloads\\geckodriver.exe");
					driver = new FirefoxDriver();
					
				}
				else{
					System.out.println("no browser value is given");
				}
				
				driver.manage().deleteAllCookies();
				driver.manage().window().maximize();
				driver.get(prop.getProperty("url"));
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
				
				driver.findElement(By.name("username")).sendKeys(prop.getProperty("username"));
				driver.findElement(By.name("password")).sendKeys(prop.getProperty("password"));
				driver.findElement(By.xpath("//input[@type='submit']")).click();
				driver.manage().timeouts().pageLoadTimeout(1000, TimeUnit.MILLISECONDS);
				//WebDriverWait wait=new WebDriverWait(driver,50);			
				//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//*[contains(text(),\"Collections\")])[2]")));
				//driver.findElement(By.cssSelector(".iconPlus")).click();
				
				//Set<String> handler=wind
				
				
				
				//Thread.sleep(3000);
				//Alert alertPrompt=driver.switchTo().alert();
				driver.findElement(By.xpath("//*[@placeholder=\"Name\"]")).sendKeys("Omni_test4");
				//alertPrompt.sendKeys("omni");
				//Thread.sleep(3000);
				//alertPrompt.accept();
				//driver.findElement(By.xpath("//*[contains(text(),\"Create\")]")).click();
				
				//driver.findElement(By.xpath("//button[@class=\"picnicButtonColorGreen picnicButton\"]")).click();
				//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				//Thread.sleep(3000);
				
				driver.findElement(By.xpath("(//*[@placeholder=\"Search...\"])[1]")).sendKeys("Omni_test1");
				driver.findElement(By.xpath("//*[contains(text(),\"Omni_test1\")]")).click();
				Thread.sleep(3000);
				//driver.findElement(By.xpath("//*[contains(text(),\"Upload\")]")).sendKeys("C:\\Users\\Arun XPS\\Desktop\\image_0");
				driver.findElement(By.xpath("//*[@type=\"file\"]")).sendKeys("C:\\Users\\Arun XPS\\dummy 3.pdf");
				Thread.sleep(5000);
				
				driver.findElement(By.xpath("(//*[@placeholder=\"Search...\"])[2]")).sendKeys("dummy 1");
	}

}
//
package CommonFuntions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Utility {

	public static void waitForElementToBePresent(WebDriver driver,String element ) {
		WebDriverWait wait=new WebDriverWait(driver,50);			
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(element)));
	}
	
	
	
}

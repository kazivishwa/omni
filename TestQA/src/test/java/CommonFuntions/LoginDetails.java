package CommonFuntions;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginDetails {
	
	
	public static void login(WebDriver driver,String userName,String password) {
		driver.findElement(By.name("username")).sendKeys(userName);
		
		driver.findElement(By.name("password")).sendKeys(password);
		
		driver.findElement(By.xpath("//input[@type='submit']")).click();
		
	}

}

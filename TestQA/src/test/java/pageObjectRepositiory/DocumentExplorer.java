package pageObjectRepositiory;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import CommonFuntions.Utility;


public class DocumentExplorer {

	
	public static WebElement addNewCollection(WebDriver driver){		 
		 WebElement element = null;		
		 Utility.waitForElementToBePresent(driver, "(//*[contains(text(),\"Collections\")])[2]");
		 driver.findElement(By.cssSelector(".iconPlus")).click();		 
		 return element;
		 
		 }
	
	public static WebElement createName(WebDriver driver,String docName) {
		WebElement element=null;
		driver.findElement(By.xpath("//*[@placeholder=\"Name\"]")).sendKeys(docName);
		return element;
	}
	public static WebElement clickToCreatCollection(WebDriver driver){
		 WebElement element = null;		 
		 driver.findElement(By.xpath("//*[contains(text(),\"Create\")]")).click();		 
		 return element;
		 
		 } 
	
	public static WebElement searchCreatedCollection(WebDriver driver,String docName){		 
		 WebElement element = null;		 
		 driver.findElement(By.xpath("(//*[@placeholder=\"Search...\"])[1]")).sendKeys(docName);
		 return element;
		 
		 } 
	
	public static WebElement selectAListFromSearchResult(WebDriver driver,String docName){		 
		 WebElement element = null;		 
		 Utility.waitForElementToBePresent(driver, "//*[contains(text(),"+docName+")]");
		 driver.findElement(By.xpath("((//table[@_ngcontent-dyh-c10]//following::tbody)[1]//td)[1]")).click();
		 return element;
		 
		 } 
	
	public static WebElement uploadFile(WebDriver driver) throws InterruptedException{		 
		 WebElement element = null;		 
		 driver.findElement(By.xpath("//*[@type=\"file\"]")).sendKeys("C:\\Users\\Arun XPS\\dummy 4.pdf");		 
		 Thread.sleep(5000);
		 return element;
		 
		 } 
	public static WebElement searchDocument(WebDriver driver) {
		WebElement element=null;
		Utility.waitForElementToBePresent(driver, "(//*[@placeholder=\"Search...\"])[2]");
		driver.findElement(By.xpath("(//*[@placeholder=\"Search...\"])[2]")).sendKeys("dummy 4");
		return element;
	}
	
	
}
